# ARSnova

Interactive Audience Response System

## Documentation

* [For developers](development.md)

## Credits

ARSnova is powered by Technische Hochschule Mittelhessen | University of Applied Sciences.
