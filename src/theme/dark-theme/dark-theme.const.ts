export const dark = {

  '--primary': '#4caf50',
  '--primary-variant': '#a5d6a7',

  '--secondary': '#ffca28',
  '--secondary-variant': '#ffecb3',

  '--warn': '#b71c1c',
  '--on-warn': '#ffffff',

  '--background': '#141414',
  '--surface': '#1e1e1e',
  '--dialog': '#2d2d2d',
  '--cancel': '#3a3a3a',
  '--alt-surface': '#323232',
  '--alt-dialog': '#2a2a2a',

  '--on-primary': '#000000',
  '--on-secondary': '#000000',
  '--on-background': '#ffffff',
  '--on-surface': '#ffffff',
  '--on-cancel': '#ffffff',

  '--table-a': '#424242',
  '--table-b': '#505050',

  '--green': '#43a047',
  '--green-light': '#e8f5e9',
  '--red': '#ff5722',
  '--red-light': '#ffccbc',
  '--yellow': '#fdd835',
  '--yellow-light': '#fff9c4',
  '--blue': '#3f51b5',
  '--grey': '#BDBDBD',
  '--black': '#000000'
};

export const dark_meta = {

  'translation': {
    'name': {
      'en': 'Dark Mode',
      'de': 'Dark Mode'
    },
    'description': {
      'en': 'Dark, battery-saving background',
      'de': 'Dunkler akkuschonender Hintergrund'
    }
  },
  'order': 3,
  'previewColor': 'background'

};
