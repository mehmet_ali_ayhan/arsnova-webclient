export const arsnova = {

  '--primary': '#5e35b1',
  '--primary-variant': '#d1c4e9',

  '--secondary': '#2e7d32',
  '--secondary-variant': '#e8f5e9',

  '--warn': '#e53935',
  '--on-warn': '#000000',

  '--background': '#f5f5f5',
  '--surface': '#ffffff',
  '--dialog': '#ffffff',
  '--cancel': '#e0e0e0',
  '--alt-surface': '#eeeeee',
  '--alt-dialog': '#eeeeee',

  '--on-primary': '#ffffff',
  '--on-secondary': '#ffffff',
  '--on-background': '#000000',
  '--on-surface': '#000000',
  '--on-cancel': '#000000',

  '--table-a': '#e0e0e0',
  '--table-b': '#eeeeee',

  '--green': '#43a047',
  '--green-light': '#e8f5e9',
  '--red': '#ff5722',
  '--red-light': '#ffccbc',
  '--yellow': '#fdd835',
  '--yellow-light': '#fff9c4',
  '--blue': '#3f51b5',
  '--grey': '#BDBDBD',
  '--black': '#000000',
};

export const arsnova_meta = {

  'translation': {
    'name': {
      'en': 'ARSnova',
      'de': 'ARSnova'
    },
    'description': {
      'en': 'Optimized for presentation in lecture halls',
      'de': 'Für die Präsentation im Hörsaal optimiert'
    }
  },
  'order': 1,
  'previewColor': 'background'

};
