import { Component } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { MatDialogRef } from '@angular/material/dialog';
import { validatePassword } from '../register/register.component';
import { AuthenticationService } from '../../../../services/http/authentication.service';
import { NotificationService } from '../../../../services/util/notification.service';
import { TranslateService } from '@ngx-translate/core';
import { EventService } from '../../../../services/util/event.service';


export class PasswordResetErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return (control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-password-reset',
  templateUrl: './password-reset.component.html',
  styleUrls: ['./password-reset.component.scss']
})
export class PasswordResetComponent {


  usernameFormControl = new FormControl('', [Validators.required, Validators.email]);
  usernameFormControl2 = new FormControl('', [Validators.required, Validators.email]);
  passwordFormControl = new FormControl('', [Validators.required]);
  passwordFormControl2 = new FormControl('', [Validators.required, validatePassword(this.passwordFormControl)]);
  keyFormControl = new FormControl('', [Validators.required]);

  matcher = new PasswordResetErrorStateMatcher();

  constructor(private translationService: TranslateService,
              public authenticationService: AuthenticationService,
              public notificationService: NotificationService,
              public dialogRef: MatDialogRef<PasswordResetComponent>,
              public eventService: EventService) {
  }


  /**
   * Closes the room create dialog on call.
   */
  closeDialog(): void {
    this.dialogRef.close();
  }


  resetPassword(username: string): void {
    username = username.trim();

    if (!this.usernameFormControl.hasError('required') && !this.usernameFormControl.hasError('email')) {
      this.authenticationService.resetPassword(username).subscribe(() => {
        this.translationService.get('password-reset.reset-successful').subscribe(message => {
          this.notificationService.show(message);
        });
        this.closeDialog();
      });
    } else {
      this.translationService.get('password-reset.input-incorrect').subscribe(message => {
        this.notificationService.show(message);
      });
    }
  }

  setNewPassword(email: string, key: string, password: string) {
    if (!this.usernameFormControl2.hasError('required') && !this.usernameFormControl2.hasError('email')
      && !this.passwordFormControl2.hasError('passwordIsEqual')) {
      if (email !== '' && key !== '' && password !== '') {
        this.authenticationService.setNewPassword(email, key, password).subscribe(() => {
          this.translationService.get('password-reset.new-password-successful').subscribe(message => {
            this.notificationService.show(message);
          });
          this.closeDialog();
        });
      } else {
        this.translationService.get('password-reset.input-incorrect').subscribe(message => {
          this.notificationService.show(message);
        });
      }
    } else {
      this.translationService.get('password-reset.input-incorrect').subscribe(message => {
        this.notificationService.show(message);
      });
    }
  }


  /**
   * Returns a lambda which closes the dialog on call.
   */
  buildCloseDialogActionCallback(): () => void {
    return () => this.closeDialog();
  }


  /**
   * Returns a lambda which executes the dialog dedicated action on call.
   */
  buildPasswordResetActionCallback(email: HTMLInputElement): () => void {
    return () => this.resetPassword(email.value);
  }
}
