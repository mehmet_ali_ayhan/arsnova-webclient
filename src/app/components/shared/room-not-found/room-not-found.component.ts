import { Component } from '@angular/core';

@Component({
  selector: 'app-room-not-found',
  templateUrl: './room-not-found.component.html',
  styleUrls: ['./room-not-found.component.scss']
})
export class RoomNotFoundComponent {

  constructor() {
  }

}
