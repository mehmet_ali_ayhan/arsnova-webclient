import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ars-screen',
  templateUrl: './full-screen-overlay.component.html',
  styleUrls: ['./full-screen-overlay.component.scss']
})
export class FullScreenOverlayComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
